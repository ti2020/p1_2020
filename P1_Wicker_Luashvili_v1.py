import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import glob
from astropy.io import fits
import scipy.signal as signal

#constantes et paramètres :
h = 6.62607004e-34
k = 1.38064852e-23
T_CMB = 2.73
c = 299792458
freq_HFI = np.array([100,143,217,353,545,857])
PSF_HFI_arcmin = np.array([9.59,7.18,4.87,4.7,4.73,4.51])
PSF_HFI = (1/60)*(np.pi/180)*PSF_HFI_arcmin

path = '/home/abeelen/Planck/maps/'

cube_hfi = sorted(glob.glob(path+'HFI_SkyMap_*.fits'))
cube_lfi = sorted(glob.glob(path+'LFI_SkyMap_*.fits'))

def degrade(): 
    """
    Degrade the maps
    """
    for i in cube_hfi:
        print(i)
        carte = hp.read_map(i)
        carte_512 = hp.ud_grade(carte, 512)
        hp.write_map('/home/wicker/data_512/'+i[len(path) : len(path)+15]+'0512'+i[len(path)+19:], carte_512, overwrite = True)
    
    for j in cube_lfi:
        carte = hp.read_map(j)
        carte_512 = hp.ud_grade(carte, 512)
        hp.write_map('/home/wicker/data_512/'+j[len(path) : len(path)+15]+'0512'+j[len(path)+19:], carte_512, overwrite = True)


def display(cube):
    """
    Display the maps
    """

    plt.ion()
    for i in cube:
        carte = hp.read_map(i)
        hp.mollview(carte, norm = 'hist')
        
"""
on pose carte = somme(signal_i*a_i + noise_i)
on veut signal_i = sommme(beta_i * carte_i)
on veut trouver les beta_i pour chaque composante (CMB, SZ etc)
c'est possible en connaissant les propiétés spectrales de chaque composante
on peut distribuer les poids beta_i à chaque carte en regardant dans les différentes cartes à disposition
De 100 à 353 GHz : en kCMB
de 545 à 857 GHz : en MJy.sr-1

=> besoin de faire la conversion en kCMB pour les plus hautes freq : voir wiki
SZ : fluctuation secondaire

"""

def conversion(NSIDE, write = False, halfmission = False):
    """
    Fait la conversion depuis les MJy.sr-1 vers kCMB
    On choisit si on travaille sur les données dégradées ou les données
    complètes
    """
    if NSIDE == 512 :
        cube = sorted(glob.glob('/home/wicker/data_512/HFI*'))
    elif NSIDE == 2048 :
        cube = sorted(glob.glob(path+'HFI_SkyMap_*'))
    print(cube)
    print(len(cube))
    N = len(hp.read_map(cube[0]))
    conv = [58.04,2.27]
    if halfmission == False :
        new_maps = np.zeros((6,N))
        for i in range(6):
            if i in [0,1,2,3]:
                new_maps[i] = hp.read_map(cube[i*3])
                if (write == True):
                    hp.write_map('/home/wicker/data_ILC/'+str(NSIDE)+'/'+cube[i*3][-35:-5]+'.fits', new_maps[i], overwrite = True)
            else :
                new_maps[i] = hp.read_map(cube[i*3]) * conv[i//5]
                if (write == True):
                    hp.write_map('/home/wicker/data_ILC/'+str(NSIDE)+'/'+cube[i*3][-35:-5]+'_converted.fits', new_maps[i], overwrite = True)
    else :
        new_maps = np.zeros((18,N))
        for i in range(18):
            if i%3 == 0:
                print('You are full of qualities')
            elif i in np.arange(12) :
                new_maps[i] = hp.read_map(cube[i])
                if (write == True):
                    hp.write_map('/home/wicker/data_ILC/halfmission/'+str(NSIDE)+'/'+cube[i][-44:-5]+'.fits', new_maps[i], overwrite = True)
            else :
                print('i//15 = ', i//15)
                new_maps[i] = hp.read_map(cube[i]) * conv[i//15]
                if (write == True):
                    hp.write_map('/home/wicker/data_ILC/halfmission/'+str(NSIDE)+'/'+cube[i][-44:-5]+'_converted.fits', new_maps[i], overwrite = True)
    return new_maps #on retourne les cartes converties pour les utiliser dans la suite


"""
MAINTENANT QUE TOUT CECI A ETE EFFECTUE,ON VA PASSER A L'IMPLEMENTATION DE L'ILC
"""
        
mix_mat = np.ndarray((2,6)) #un vecteur par composante à isoler, chaque vecteur est long comme le nombre de cartes utilisées
#pour l'instant, on s'intéressera à un modele à deux composantes seulement
#on va encoder la réponse spectrale de chacune des composantes dans la matrice de mélange, en faisant correspondre un vecteur de mélange à chaque composante

CMB_comp = mix_mat[0]
tSZ_comp = mix_mat[1]

def prep_maps(NSIDE, halfmission = False):
    if halfmission == False :
        if NSIDE == 512 :
            cube_ILC = sorted(glob.glob('/home/wicker/data_ILC/512/*.fits'))
        elif NSIDE == 2048 :
            cube_ILC = sorted(glob.glob('/home/wicker/data_ILC/2048/*.fits'))
        N = len(hp.read_map(cube_ILC[0]))
        maps = np.zeros((6,N))
    else:
        if NSIDE == 512 :
            cube_ILC = sorted(glob.glob('/home/wicker/data_ILC/halfmission/512/*.fits'))
        elif NSIDE == 2048 :
            cube_ILC = sorted(glob.glob('/home/wicker/data_ILC/halfmission/2048/*.fits'))
        N = len(hp.read_map(cube_ILC[0]))
        maps = np.zeros((12,N))
    for i in range(len(cube_ILC)):
        maps[i] = hp.read_map(cube_ILC[i])
    return maps, N

def reponse_tSZ(nu):
    """
    Réponse spectrale de l'effet tSZ
    """
    x = h*nu/(k*T_CMB)
    return x*(np.exp(x)+1)/(np.exp(x)-1) - 4

def masque_gal(NSIDE, mapsarray, thresh):
    """
    On masque la contribution parasite de la galaxie
    """
    if NSIDE == 512 :
        masque = np.zeros(3145728)
    if NSIDE == 2048 :
        masque = np.zeros(50331648)
    masque[mapsarray[5] > thresh] = 1
    masked = np.ma.array(mapsarray, mask = [6*[masque]])
    masked.fill_value = np.nan
#    hp.mollview(masque)
    return masked

def conv_degrade(NSIDE, halfmission = False):
    """
    On veut dégrader la résolution des cartes à plus
    haute fréquence afin d'avoir des résolutions
    équivalentes quand on soustraiera les cartes.
    Pour ceci on va fitter la PSF de la carte à plus basse
    résolution (100GHz) avec une gaussienne, qu'on convoluera
    aux cartes à dégrader
    """
    maps, N = prep_maps(NSIDE, halfmission)
    print(len(maps))
    smoothed = np.zeros((len(maps),N))
    for i in range(len(maps)):
        smoothed[i] = hp.sphtfunc.smoothing(maps[i], fwhm = np.sqrt(PSF_HFI[0]**2 - PSF_HFI[i//2]**2))
        print("Until here I'm okay !", i)
        print("Hey ! Everything may have messed up because of me !", i//2)
    return smoothed, N


def indep_ILC(NSIDE, halfmission_1 = False, halfmission_2 = False):
    """
    C'est ici qu'on va faire notre ILC. On commence par 
    initialiser nos vecteurs de mélange, contenant la réponse
    spectrale de chaque composante. Ensuite on pourra utiliser
    la formule de l'ILC
    """
    #on initialise d'abord la composante du CMB
    CMB_comp = np.ones(6)
    
    #On produit le masque
    fullness = 'full'
    if halfmission_1 == True :
        smoothed, N = conv_degrade(NSIDE, halfmission = True)
        print(smoothed[np.arange(5, step = 2)])
        smoothed = smoothed[np.arange(11, step = 2)]
        fullness = 'halfmission_1'
    elif halfmission_2 == True :
        smoothed, N = conv_degrade(NSIDE, halfmission = True)
        smoothed = smoothed[np.arange(1, 12, step = 2)]
        fullness = 'halfmission_2'
    else :
        smoothed, N = conv_degrade(NSIDE)
    masked = masque_gal(NSIDE,smoothed, 10)

    #ensuite on s'occupe de la composante de tSZ
    conv_SZ = [-0.24815, -0.35923, 5.152, 0.161098, 0.06918, 0.0380] #on passe la réponse en freq du SZ en unités de kCMB
    for i in range(6):
        nu = freq_HFI[i]
        print("nu = ", nu, "GHz")
        tSZ_comp[i] = conv_SZ[i] * reponse_tSZ(nu*1e9)
        hp.mollview(masked[i], norm = 'hist', cmap = 'Spectral')

#    print (tSZ_comp)

    cov_mat = np.cov(masked)
    print('shape cmb_comp transp = ', np.shape(CMB_comp.T))
    print('shape tsz_comp transp = ', np.shape(tSZ_comp.T))
    print('shape masked = ', np.shape(masked))
    print('shape maps = ', np.shape(smoothed))
    print('shape cov mat = ', np.shape(cov_mat))
    print('shape of cov_mat-1 = ', np.shape(np.linalg.inv(cov_mat)))
    signal_CMB = np.dot(np.dot(CMB_comp.T, np.linalg.inv(cov_mat)), masked) / (CMB_comp.T @ np.linalg.inv(cov_mat) @ CMB_comp)
    signal_tSZ = np.dot(np.dot(tSZ_comp.T, np.linalg.inv(cov_mat)), masked) / (tSZ_comp.T @ np.linalg.inv(cov_mat) @ tSZ_comp)
    
    print('shape of signal CMB = ', np.shape(signal_CMB))
    hp.mollview(signal_CMB, norm = 'hist', cmap = 'Spectral')
    plt.savefig('CMB_map_'+str(NSIDE)+'_'+fullness+'.png')
    hp.mollview(signal_tSZ, norm = 'hist', cmap = 'Spectral')
    plt.savefig('tSZ_map_'+str(NSIDE)+'_'+fullness+'.png')
    hp.write_map('/home/wicker/final_maps/CMB_'+str(NSIDE)+'_'+fullness+'.fits', signal_CMB, overwrite = True)
    hp.write_map('/home/wicker/final_maps/tSZ_'+str(NSIDE)+'_'+fullness+'.fits', signal_tSZ, overwrite = True)
    return signal_CMB, signal_tSZ

def spectre_CMB(NSIDE, crossing = False):
    """
    Pour le spectre de reference, on a 2HDU, qu'on va sélectionner un par un, dans lequels sont
    rangés les données sous forme de dictionnaire
    'fullness' détermine quel set de données on va sélectionner. On peut lui donner les valeurs suivantes
    - 'full'
    - 'halfmission_1'
    - 'halfmission_2'
    """
    if NSIDE == 512 :
        map_857 = hp.read_map('/home/wicker/data_512/HFI_SkyMap_857_0512_R2.00_full.fits')
        N = 3145728
    if NSIDE == 2048 :
        map_857 = hp.read_map('/home/abeelen/Planck/maps/HFI_SkyMap_857_2048_R2.00_full.fits')
        N = 50331648
        
    thresh = 10
    masque = np.zeros(N)
    masque[map_857 > thresh] = 1
    f_sky = 1 - len(masque[map_857 > thresh])/len(masque)
    print('fsky = ', f_sky)
    
    if crossing == True :
        CMB_map_1 = hp.read_map('/home/wicker/final_maps/CMB_'+str(NSIDE)+'_halfmission_1.fits')
        CMB_map_2 = hp.read_map('/home/wicker/final_maps/CMB_'+str(NSIDE)+'_halfmission_2.fits')
        masked_CMB_1 = np.ma.array(CMB_map_1, mask = masque)
        masked_CMB_2 = np.ma.array(CMB_map_2, mask = masque)
        masked_CMB_1.fill_value = 0
        masked_CMB_2.fill_value = 0
    else :
        CMB_map = hp.read_map('/home/wicker/final_maps/CMB_'+str(NSIDE)+'_full.fits')
        masked_CMB = np.ma.array(CMB_map, mask = masque)
        masked_CMB.fill_value = 0
    CMB_ref1 = fits.getdata('/home/wicker/final_maps/COM_PowerSpect_CMB_R2.00.fits', 1) #1er patch du spectre de ref (1er HDU)
    CMB_ref2 = fits.getdata('/home/wicker/final_maps/COM_PowerSpect_CMB_R2.00.fits', 2) #2e patch du spectre de ref (2e HDU)
    CMB_ref = np.concatenate((CMB_ref1['D_ELL'], CMB_ref2['D_ELL'])) #on sélectionne les valeurs de l'intensité du spectre dans le dictionnaire


    LMAX = 2048
    ell1 = np.arange(2049)
    ell2 = np.arange(2049)
    corr = (hp.sphtfunc.gauss_beam(PSF_HFI[0], lmax = LMAX)**2 * hp.sphtfunc.pixwin(NSIDE)[:2049]**2)
#    corr_map = hp.synfast(corr, NSIDE)
    print (len(corr))
    masked_corr = f_sky*corr
    if crossing == True :
        cl1 = hp.anafast(CMB_map_1 , CMB_map_2 , lmax=LMAX)*1e12/(2*np.pi*corr)
        cl2 = hp.anafast(masked_CMB_1 , masked_CMB_2 , lmax=LMAX)*1e12/(2*np.pi*masked_corr)
    else :
        cl1 = hp.anafast(CMB_map, lmax=LMAX)*1e12/(2*np.pi)
        cl2 = hp.anafast(masked_CMB, lmax=LMAX)*1e12/(2*np.pi)
        cl1 = cl1/corr
        cl2 = cl2/masked_corr
    
    ell3 = np.concatenate((CMB_ref1['ELL'], CMB_ref2['ELL'])) #on sélectionne les valeurs de l, et on concatene les deux patchs
#    CMB_ref = CMB_ref * (hp.sphtfunc.gauss_beam(PSF_HFI[0], lmax = 2499)[ell3])**2
#    print (ell2)
    
#    print (CMB_ref)
#    print (cl)
    
    fig1 = plt.figure(figsize=(10, 5))
    
    ax1 = fig1.add_subplot(111)
    ax1.plot(ell1, cl1 * ell1 * (ell1 + 1), label = "Students' map, unmasked CMB")
    ax1.plot(ell2, cl2 * ell2 * (ell2 + 1), label = "Students' map, masked CMB")
    ax1.plot(ell3, CMB_ref, label = 'Reference map')
    plt.xlabel("$\ell$")
    plt.ylabel("$\ell(\ell+1)C_{\ell}$")
    plt.grid()
    ax1.legend(loc = 'lower right')
    if crossing == False :
        plt.title('CMB power spectrum for NSIDE = '+str(NSIDE)+' with mask thresh = '+str(thresh)+', $f_{sky} = $ '+str(f_sky))
        plt.savefig('CMB_pow_spec_'+str(NSIDE)+'_'+str(thresh)+'_full.png', overwrite = True)
    else :
        plt.title('CMB cross-spectrum for NSIDE = '+str(NSIDE)+' with mask thresh = '+str(thresh)+', $f_{sky} = $ '+str(f_sky))
        plt.savefig('CMB_pow_spec_'+str(NSIDE)+'_'+str(thresh)+'_cross.png', overwrite = True)
    
#    ax2 = fig.add_subplot(212)
#    rel_dif = CMB_ref - cl*(ell * (ell + 1))
#    ax2.plot(ell, rel_dif, label = 'Relative difference between the two')
#    plt.xlabel("$\ell$")
#    plt.ylabel(r"$\frac{Exp - Ref}{Ref}$")
#    hp.write_cl('/home/wicker/final_maps/pow_spec.fits', cl, overwrite=True)
#    ax2.legend(loc = 'lower right')
